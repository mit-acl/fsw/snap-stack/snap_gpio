/**
 * @file snap_gpio.h
 * @brief Snapdragon GPIO API
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 March 2020
 */

#pragma once

#include <cstdint>

namespace acl {

  class SnapGPIO
  {
  public:
    SnapGPIO(uint8_t gpio);
    ~SnapGPIO();

    bool configureAsInput();
    bool configureAsOutput();

    bool setHigh();
    bool setLow();

    bool isHigh();
    bool isLow();

  private:
    uint8_t gpio_num_; ///< GPIO number being managed
    int gpio_; ///< GPIO file descriptor being managed
  };

} // ns acl
