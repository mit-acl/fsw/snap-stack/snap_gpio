/**
 * @file snap_gpio_set.cpp
 * @brief CLI tool to set a GPIO as HIGH/LOW
 * @author Parker Lusk <plusk@mit.edu>
 * @date 5 August 2020
 */

#include <cstring>
#include <iostream>
#include <sstream>
#include <thread>

#include "snap_gpio/snap_gpio.h"

int main(int argc, char const *argv[])
{
  // make sure the user understands how to use this tool
  if (argc != 3) {
    std::cout << "usage: " << argv[0] << " <gpio> <highlow>" << std::endl;
    std::cout << "\t gpio: GPIO number 0-149 (see schematics)" << std::endl;
    std::cout << "\t highlow: 0 for LOW, 1 for HIGH" << std::endl;
    std::cout << std::endl;
    return -1;
  }

  // extract GPIO number from argument
  int gpio_num;
  {
    std::istringstream ss(argv[1]);
    if (!(ss >> gpio_num)) {
      std::cerr << "Invalid GPIO number: " << argv[1] << std::endl;
    }
  }

  // extract HIGH/LOW mode from argument
  bool high;
  {
    int tmp;
    std::istringstream ss(argv[2]);
    if (!(ss >> tmp)) {
      std::cerr << "Invalid highlow number: " << argv[2] << std::endl;
    } else {
      high = tmp == 1;
    }
  }

  //
  // Set the GPIO
  //

  acl::SnapGPIO gpio(gpio_num);
  gpio.configureAsOutput();
    
  if (high) gpio.setHigh();
  else gpio.setLow();

  return 0;
}
