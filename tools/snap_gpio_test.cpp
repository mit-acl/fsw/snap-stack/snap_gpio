/**
 * @file snap_gpio_test.cpp
 * @brief CLI tool to interact with GPIO
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 March 2020
 */

#include <cmath>
#include <chrono>
#include <csignal>
#include <cstring>
#include <iostream>
#include <sstream>
#include <thread>

#include "snap_gpio/snap_gpio.h"

// to be set by sighandler
volatile sig_atomic_t stop = 0;
void handle_sigint(int s) { stop = true; }

int main(int argc, char const *argv[])
{
  // install sig handler
  struct sigaction sa;
  memset(&sa, 0, sizeof(struct sigaction));
  sa.sa_handler = handle_sigint;
  sa.sa_flags = 0; // not SA_RESTART
  sigaction(SIGINT, &sa, NULL);


  // make sure the user understands how to use this tool
  if (argc != 3) {
    std::cout << "usage: " << argv[0] << " <gpio> <inout>" << std::endl;
    std::cout << "\t gpio: GPIO number 0-149 (see schematics)" << std::endl;
    std::cout << "\t inout: 0 for input, 1 for output" << std::endl;
    std::cout << std::endl;
    return -1;
  }

  // extract GPIO number from argument
  int gpio_num;
  {
    std::istringstream ss(argv[1]);
    if (!(ss >> gpio_num)) {
      std::cerr << "Invalid GPIO number: " << argv[1] << std::endl;
    }
  }

  // extract input/output mode from argument
  bool input;
  {
    int tmp;
    std::istringstream ss(argv[2]);
    if (!(ss >> tmp)) {
      std::cerr << "Invalid inout number: " << argv[2] << std::endl;
    } else {
      input = tmp == 0;
    }
  }

  //
  // Run the test
  //

  acl::SnapGPIO gpio(gpio_num);
  if (input) gpio.configureAsInput();
  else gpio.configureAsOutput();

  constexpr size_t NUM_CYCLES = 10;
  constexpr size_t Tms_WAIT = 100;
  for (size_t i=0; i<NUM_CYCLES; ++i) {
    
    // if GPIO is an output, toggle it
    if (!input) {
      if (gpio.isHigh()) gpio.setLow();
      else gpio.setHigh();
    }

    std::cout << ((gpio.isHigh())?"true":"false") << std::endl;
    
    // sleep for a bit
    std::this_thread::sleep_for(std::chrono::milliseconds(Tms_WAIT));

    // allow clean early termination
    if (stop) break;
  }

  return 0;
}
