/**
 * @file snap_gpio.cpp
 * @brief Snapdragon GPIO API
 * @author Parker Lusk <plusk@mit.edu>
 * @date 4 March 2020
 */

#include "snap_gpio/snap_gpio.h"

#include "gpio.h"

namespace acl {

SnapGPIO::SnapGPIO(uint8_t gpio)
: gpio_num_(gpio)
{

  gpio_ = gpio_open(gpio_num_);
}

// ----------------------------------------------------------------------------

SnapGPIO::~SnapGPIO()
{
  gpio_close(gpio_);
}

// ----------------------------------------------------------------------------

bool SnapGPIO::configureAsInput()
{
  return gpio_configure(gpio_, 0) == 0;
}

// ----------------------------------------------------------------------------

bool SnapGPIO::configureAsOutput()
{
  return gpio_configure(gpio_, 1) == 0;
}

// ----------------------------------------------------------------------------

bool SnapGPIO::setHigh()
{
  return gpio_write(gpio_, 1) == 0;
}

// ----------------------------------------------------------------------------

bool SnapGPIO::setLow()
{
  return gpio_write(gpio_, 0) == 0;
}

// ----------------------------------------------------------------------------

bool SnapGPIO::isHigh()
{
  return gpio_read(gpio_) == 1;
}

// ----------------------------------------------------------------------------

bool SnapGPIO::isLow()
{
  return gpio_read(gpio_) == 0;
}

} // ns acl
