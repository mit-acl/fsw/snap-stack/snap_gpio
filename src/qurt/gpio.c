/**
 * @file sfpro_pwm_imp.c
 * @brief DSP implementation of PWM/ESC interface for sfpro 8096 using pca9685
 * @author Parker Lusk <plusk@mit.edu>
 * @date 27 June 2019
 */

#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdint.h>
#include <string.h>
#include <sys/ioctl.h>
#include <dspal_platform.h>
#include <dev_fs_lib_gpio.h>

#include "utils.h"

// #if SFPRO
// #define DEV_STRING DEV_FS_GPIO_SSC_DEVICE_TYPE_STRING
// #else
// #define DEV_STRING DEV_FS_GPIO_DEVICE_TYPE_STRING
// #endif

#define DEV_STRING DEV_FS_GPIO_DEVICE_TYPE_STRING

/**
 * See https://github.com/quest-fw/quest-fw/blob/quest-master/SnapdragonFlight/DspRelay/gpio_relay.c
 */

int gpio_open(int gpio)
{
  /* Request 100% of max clock speed, 100% of max bus speed, and max of 1us
  * hardware wakeup latency
  * See:
  * /opt/tools/leo/Qualcomm/Hexagon_SDK/3.0/incs/HAP_power.h
  * https://github.com/PX4/Firmware/blob/master/src/modules/muorb/adsp/px4muorb.cpp#L58
  */
  HAP_power_request(100, 100, 1);

  char dev[256];
  snprintf(dev, sizeof(dev), DEV_STRING "%d", gpio);
  LOG_INFO("[gpio] Attempting to open GPIO device %s", dev);
  int fd = open(dev, 0);
  if (fd == -1) {
    LOG_ERR("[gpio] Failed to open %s: %s", dev, strerror(errno));
    return GPIO_ERROR;
  }
  LOG_INFO("[gpio] Successfully opened GPIO device %s: fd %d", dev, fd);
  return fd;
}

// ----------------------------------------------------------------------------

int gpio_close(int fd)
{
  LOG_INFO("[gpio] Closing fd %d", fd);
  return close(fd);
}

// ----------------------------------------------------------------------------

int gpio_configure(int fd, int type)
{

  struct dspal_gpio_ioctl_config_io config = {
    .direction = (enum DSPAL_GPIO_DIRECTION_TYPE)type,
    .pull = DSPAL_GPIO_NO_PULL,
    .drive = DSPAL_GPIO_2MA,
  };

  // configure GPIO device into general purpose IO mode
  if (ioctl(fd, DSPAL_GPIO_IOCTL_CONFIG_IO, (void *)&config) != 0) {
    LOG_ERR("[gpio] ioctl fd %d failed: %s", fd, strerror(errno));
    return GPIO_ERROR;
  }

  LOG_INFO("[gpio] fd %d successfully configured", fd);
  return GPIO_SUCCESS;
}

// ----------------------------------------------------------------------------

int gpio_write(int fd, int val)
{

  enum DSPAL_GPIO_VALUE_TYPE value_written = (enum DSPAL_GPIO_VALUE_TYPE)val;

  // set output value
  int bytes = write(fd, &value_written, 1);
  if (bytes != 1) {
      LOG_ERR("[gpio] fd %d write failed: %d: %s", fd, bytes, strerror(errno));
      return bytes;
  }
    
  LOG_INFO("[gpio] fd %d write successful (%d)",fd,val);
  return GPIO_SUCCESS;
}

// ----------------------------------------------------------------------------

int gpio_read(int fd)
{

  enum DSPAL_GPIO_VALUE_TYPE value_read;

  // verify the write result by reading the output from the same GPIO
  int bytes = read(fd, &value_read, 1);

  if (bytes != 1) {
    LOG_ERR("[gpio] fd %d read failure: %d: %s", fd, bytes, strerror(errno));
    return GPIO_ERROR;
  }

  LOG_INFO("[gpio] fd %d read value %u", fd, value_read);

  /* NOTE(mereweth) - observed weird case on 801 where DSP gpio
  * read value was large number, odd or even to indicate state.
  * even is low, odd is high
  */
  return (value_read % 2);
}
