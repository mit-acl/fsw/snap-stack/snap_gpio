Snapdragon GPIO Interface
=========================

This project manages GPIO signaling on the DSP and exposes a CPU-side shared library for linking to other CPU projects (e.g., ROS).
Implementation details inspired by [quest-fw](https://github.com/quest-fw/quest-fw/blob/quest-master/SnapdragonFlight/DspRelay/gpio_relay.c).

## Building

1. Clone into the [`sfpro-dev`](https://github.com/mit-acl/sfpro-dev) or [`sf-dev`](https://github.com/mit-acl/sf-dev) `workspace` directory.
2. Update submodules: `git submodule update --init --recursive`.
3. Follow appropriate instructions for building and pushing via `adb`: `./build.sh workspace/snap_gpio --load`.

## Which GPIOs?

The GPIOs are numbered as shown in the sf and sfpro schematics (see link in `sf-dev` or `sfpro-dev`). On the sfpro, the GPIOs on the Snapdragon Sensor Core (SSC) could also be selected, however this is [commented out](src/qurt/gpio.c#L20) to default to the main GPIOs since the SSC GPIOs are used for peripherals.

Another good place to look for which pins correspond to which GPIOs is [here](https://developer.qualcomm.com/hardware/qualcomm-flight-pro/board-pin-outs) sfpro and [here](https://docs.px4.io/v1.9.0/en/flight_controller/snapdragon_flight.html) for the sf.
