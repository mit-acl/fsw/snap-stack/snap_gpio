all: snap_gpio

QC_SOC_TARGET?=APQ8074

# choose cpu target based on target board
cpu:=
ifeq ($(QC_SOC_TARGET),APQ8074)
	cpu:=krait
else
	cpu:=kyro
endif

.PHONY: ENV_VARS
ENV_VARS:
	@[ ! -z "${HEXAGON_SDK_ROOT}" ] || (echo "HEXAGON_SDK_ROOT not set" && false)
	@[ ! -z "${HEXAGON_TOOLS_ROOT}" ] || (echo "HEXAGON_TOOLS_ROOT not set" && false)

.PHONY: krait
krait: ENV_VARS
	mkdir -p build/krait && cd build/krait && cmake -Wno-dev ../.. -DQC_SOC_TARGET=${QC_SOC_TARGET} -DCMAKE_TOOLCHAIN_FILE=lib/cmake_hexagon/toolchain/Toolchain-arm-linux-gnueabihf.cmake
	cd build/krait && make

.PHONY: kyro
kyro: ENV_VARS
	mkdir -p build/kyro && cd build/kyro && cmake -Wno-dev ../.. -DQC_SOC_TARGET=${QC_SOC_TARGET} -DCMAKE_TOOLCHAIN_FILE=lib/cmake_hexagon/toolchain/Toolchain-arm-oemllib32-linux-gnueabi.cmake
	cd build/kyro && make

.PHONY: qurt
qurt: ENV_VARS
	mkdir -p build/qurt && cd build/qurt && cmake -Wno-dev ../.. -DQC_SOC_TARGET=${QC_SOC_TARGET} -DCMAKE_TOOLCHAIN_FILE=lib/cmake_hexagon/toolchain/Toolchain-qurt.cmake
	cd build/qurt && make

.PHONY: snap_gpio
snap_gpio: $(cpu) qurt ;
	
load: snap_gpio
	cd build/$(cpu) && make snap_gpio-load
	cd build/qurt && make libsnap_gpio_qurt-load

clean:
	rm -rf build